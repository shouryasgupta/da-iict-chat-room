package chatclient;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ChatClient implements Runnable {
    
  static boolean Run = false;
  static DataInputStream in = null;
 
  public void run() {
    
    String reply;
    try {
      while ((reply = in.readLine()) != null) {
        System.out.println(reply);
        if (reply.equals("NAME"))
            continue;
        if (reply.contains("GoodBye"))
          break;
      }
      Run = true;
    } catch (IOException e) {
      System.err.println("IOException !!!" );
    }
  }
  
  public static void main(String[] args) {
    
    BufferedReader read = null;
    Socket clientSocket = null;
    PrintStream out = null;
  
    try {
      clientSocket = new Socket("localhost", 4444);
      read = new BufferedReader(new InputStreamReader(System.in));
      out = new PrintStream(clientSocket.getOutputStream());
      in = new DataInputStream(clientSocket.getInputStream());
      System.out.println("You are now connected");
    } catch (UnknownHostException e) {
            System.err.println("Don't know about host: localhost.");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + "the connection to: localhost.");
            System.exit(1);
    }

      try {
        new Thread(new ChatClient()).start();
        while (!Run) {
          out.println(read.readLine());
        }
        in.close();
        out.close();
        clientSocket.close();
      } catch (IOException e) {
        System.err.println("errr!!! IOException ");
      }
  }

 
}