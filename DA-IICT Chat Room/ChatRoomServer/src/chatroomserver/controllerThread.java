package chatroomserver;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

class controllerThread extends Thread {

  String name = null;
  Socket clientSocket = null;
  final controllerThread[] threads;
  DataInputStream in = null;
  PrintStream out = null;
  int maxSlots;

  public void run() {
    int maxSlots = this.maxSlots;
    controllerThread[] threads = this.threads;

    try {
        
      in = new DataInputStream(clientSocket.getInputStream());
      out = new PrintStream(clientSocket.getOutputStream());
      BufferedReader stdIn =new BufferedReader(new InputStreamReader(System.in));
      out.println("Please Identify Yourself :)");
      this.name = in.readLine();
      out.println("Welcome " + name+ " to DA-IICT Chat Room. Please maintain the decorum of this chat room !!\nJust Type message in console to multicast message to all members in chat\nType PRIVATE to send a personal message\nType NAME to list all members in chat room\nType EXIT to quit chat room");
      for (int i = 0; i < maxSlots; i++) {
        if (threads[i] != null && threads[i] != this) {
          threads[i].out.println(name + " entered the DA-IICT chat room !!! ");
        }
      }
      while (true) {
        int counter=0;
        String input = in.readLine();
        
        if(input.equals("PRIVATE")){
            for (int i = 0; i < maxSlots; i++) 
            {
                if (threads[i] != null && threads[i]!=this) {
                    out.println("To send message to "+threads[i].name+" type "+i+"\n");
                }
            }    
            String entry=null;
            int num=0;
            while((entry=in.readLine()).equals(""));
            num=Integer.parseInt(entry);
            out.println("Enter the message to send to "+threads[num].name);
            while((entry=in.readLine()).equals(""));
            threads[num].out.println("<PRIVATE>" + name +" : " + entry);
            continue;
        }
        
        if(input.equals("NAME"))
        {
            out.println("Members Currently in Chat Room Are : \n");
            for (int i = 0; i < maxSlots; i++) 
            {
                if (threads[i] != null) {
                    out.println(threads[i].name+"\n");
                    counter++;
                }
            }
            if(counter==0)
            {
                out.println("There are no members currently in chat room\n");
            }
            continue;
        }
        
        if (input.equals("EXIT")) {
          break;
        }
        for (int i = 0; i < maxSlots; i++) {
          if (threads[i] != null && threads[i]!=this) {
            threads[i].out.println("<MULTICAST>" + name + " : " + input);
          }
        }
      }
      for (int i = 0; i < maxSlots; i++) {
        if (threads[i] != null && threads[i] != this) {
          threads[i].out.println(name+" has left DA-IICT chat room.");
        }
      }
      out.println("GoodBye "+name);

      for (int i = 0; i < maxSlots; i++) {
        if (threads[i] == this) {
          threads[i] = null;
        }
      }

      in.close();
      out.close();
      clientSocket.close();
    } catch (IOException e) {
        System.out.println("IOExceptio err !!! ");
    }
  }
  public controllerThread(Socket clientSocket, controllerThread[] threads) {
    maxSlots = threads.length;
    this.clientSocket = clientSocket;
    this.threads = threads;
  }
}