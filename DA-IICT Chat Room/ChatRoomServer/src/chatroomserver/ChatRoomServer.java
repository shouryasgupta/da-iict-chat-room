package chatroomserver;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.io.IOException;


public class ChatRoomServer {
  
    public static void main(String args[]) {
    int  maxSlots = 15 ;
    controllerThread[] threads = new controllerThread[maxSlots];
    ServerSocket serverSocket=null;
    Socket clientSocket=null;
    try {
        serverSocket = new ServerSocket(4444);
        System.out.println("Server Socket Created");
    } catch (IOException e) {
      System.out.println(e);
    }

    while (true) {
      try {
        clientSocket = serverSocket.accept();
        int i = 0;
        for (i = 0; i < maxSlots; i++) {
          if (threads[i] == null) {
            (threads[i] = new controllerThread(clientSocket, threads)).start();
            break;
          }
        }
        if (i == maxSlots) {
          PrintStream os = new PrintStream(clientSocket.getOutputStream());
          os.println("Server OverLoaded Currently !!! ");
          os.close();
          clientSocket.close();
        }
      } catch (IOException e) {
        System.out.println(e);
      }
    }
  }
}
